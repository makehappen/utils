<?php
namespace MakeHappen\Utils;

/**
    Sanitizes, Filters, and Validates incoming data from GET and POST
    Creates arrErrors array with validation errors

    Usage Example:

    $objRequest = new MakeHappen\Utils\Request();
    $intId      = $objRequest->getParam('id', 0, ['type'=>'int']);
    $strValue   = $objRequest->getParam('string', '', ['required'=>false]);

    // reference validation errors
    $arrErrors = $objFormData->arrErrors();
*/

class Request
{

    function __construct()
    {
        $this->arrErrors = [];
    }

    function getParam($key = null, $default = null, $arrOptions = [])
    {

        $arrOptions = [
            'filter'    => isset($arrOptions['filter'])     ? $arrOptions['filter']     : true,
            'message'   => isset($arrOptions['message'])    ? $arrOptions['message']    : null,
            'required'  => isset($arrOptions['required'])   ? $arrOptions['required']   : true,
            'type'      => isset($arrOptions['type'])       ? $arrOptions['type']       : '',
            'validate'  => isset($arrOptions['validate'])   ? $arrOptions['validate']   : true

        ];

        if (isset( $_POST[$key])){
            $mixParamValue = $_POST[$key];
        } elseif (isset( $_GET[$key])) {
            $mixParamValue = $_GET[$key];
        } else {
            $mixParamValue = null;
        }

        if ($mixParamValue === null){
            // default may be 0, emtpy, or false; if so return it instead of null
            return ($default !== null) ?  $default : $mixParamValue;
        }

        if (is_array($mixParamValue)){
            foreach ($mixParamValue as $index => $value){
                $mixParamValue[$index] = $this->validateParam($key, $default, $arrOptions, $value, $index);
            }
        } else {
            $mixParamValue = $this->validateParam($key, $default, $arrOptions, $mixParamValue);
        }

        return $mixParamValue;

    }

    function validateParam( $key = null, $default = null, $arrOptions = [] , $mixParamValue = null, $index = null)
    {

        // trim input
        $mixParamValue = trim($mixParamValue);

        // required
        if ($arrOptions['required'] && !$mixParamValue && $mixParamValue !== 0 && $mixParamValue !== '0'){
            ($index === null)
                ? $this->arrErrors[$key] = $arrOptions['message'] ?: 'Required field'
                : $this->arrErrors[$key][$index] = $arrOptions['message'] ?: 'Required field';
        }

        // validate
        if ($arrOptions['validate'] && $mixParamValue){

            switch ($arrOptions['type']){

                case 'email':
                    if(!filter_var($mixParamValue, FILTER_VALIDATE_EMAIL)){
                        ($index === null)
                            ? $this->arrErrors[$key] = $arrOptions['message'] ?: "Invalid Email Address"
                            : $this->arrErrors[$key][$index] = $arrOptions['message'] ?: "Invalid Email Address";
                    }
                    break;

                case 'url':
                    if(!filter_var($mixParamValue, FILTER_VALIDATE_URL)){
                        ($index === null)
                            ? $this->arrErrors[$key] = $arrOptions['message'] ?: "Invalid URL"
                            : $this->arrErrors[$key][$index] = $arrOptions['message'] ?: "Invalid URL";
                    }
                    break;

                case 'ip':
                    if(!filter_var($mixParamValue, FILTER_VALIDATE_IP)){
                        ($index === null)
                            ? $this->arrErrors[$key] = $arrOptions['message'] ?: "Invalid IP address"
                            : $this->arrErrors[$key][$index] = $arrOptions['message'] ?: "Invalid IP address";
                    }
                    break;

                case 'int':
                case 'number':
                    if(!filter_var($mixParamValue, FILTER_VALIDATE_INT)){
                        ($index === null)
                            ? $this->arrErrors[$key] = $arrOptions['message'] ?: "Invalid numerical value. Not an integer."
                            : $this->arrErrors[$key][$index] = $arrOptions['message'] ?: "Invalid numerical value. Not an integer.";
                    }
                    break;

                case 'float':
                    if(!filter_var($mixParamValue, FILTER_VALIDATE_FLOAT)){
                        ($index === null)
                            ? $this->arrErrors[$key] = $arrOptions['message'] ?: "Invalid numerical value. Not a decimal number."
                            : $this->arrErrors[$key][$index] = $arrOptions['message'] ?: "Invalid numerical value. Not a decimal number.";
                    }
                    break;

                case 'boolean':
                    if(NULL === filter_var($mixParamValue, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE)){
                        ($index === null)
                            ? $this->arrErrors[$key] = $arrOptions['message'] ?: "Invalid boolean value. '0', 'false', 'off', 'no', and '' are accepted values."
                            : $this->arrErrors[$key][$index] = $arrOptions['message'] ?: "Invalid boolean value. '0', 'false', 'off', 'no', and '' are accepted values.";
                    }
                    break;

                case 'date':
                    if(!strtotime($mixParamValue)){
                        ($index === null)
                            ? $this->arrErrors[$key] = $arrOptions['message'] ?: "Invalid date"
                            : $this->arrErrors[$key][$index] = $arrOptions['message'] ?: "Invalid date";
                    }
                    break;

            }

        }

        // apply filter
        if ($arrOptions['filter']){

            switch($arrOptions['type']){

                case 'email':
                    $mixParamValue = filter_var($mixParamValue, FILTER_SANITIZE_EMAIL);
                    break;

                case 'int':
                case 'number':
                case 'credit_card':
                    $mixParamValue = preg_replace("/[^0-9]/",'',$mixParamValue);
                    break;

                case 'url':
                    $mixParamValue = filter_var($mixParamValue, FILTER_SANITIZE_ENCODED);
                    break;

                case 'date':
                    $mixParamValue = $mixParamValue ? date('Y-m-d',strtotime($mixParamValue)) : $mixParamValue;
                    break;

                case 'boolean':
                    $mixParamValue = filter_var($mixParamValue, FILTER_VALIDATE_BOOLEAN);
                    break;

                case 'text':
                case 'string':
                    $mixParamValue = filter_var($mixParamValue, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
                    break;

                default:
                    $mixParamValue = filter_var($mixParamValue, FILTER_SANITIZE_STRING);

            }

        }

        // $default must be set to a value that's true as boolean to be returned
        // otherwise return the incoming value ( may be 0 or emtpy string which are boolean false )
        return (!$mixParamValue && $default) ?  $default : $mixParamValue;

    }

}